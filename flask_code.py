from config import SHOW_THRESHOLD, SAVE_THRESHOLD
from database_code import DataBase
from image_processing import MNIST_model
from flask import Flask, render_template, request
import string
import random
import base64


app = Flask(__name__)
db = DataBase()
mnist_model = MNIST_model()
predicted_numbers = {}
encoded_image = ''


@app.route('/index')
@app.route('/')
def index():
    return render_template('index.html')


@app.route('/canvas', methods=('GET', 'POST'))
def canvas():
    char_set = string.ascii_uppercase + string.digits
    global predicted_numbers, encoded_image

    if request.method == 'POST':
        for request_key in request.values.keys():

            if request_key == 'imageBase64':
                encoded_image = request.values['imageBase64']
                if encoded_image:
                    encoded_image = encoded_image.split(',')[1]
                    predicted_numbers = mnist_model.predict(encoded_image)
                    text_to_show = ''
                    enable_save = '0;'

                    for number in predicted_numbers:
                        if predicted_numbers[number] > SHOW_THRESHOLD:
                            text_to_show += 'Number ' + str(number) + ' with probability ' + str(round(predicted_numbers[number], 3)) + '%<br>'
                    if max(predicted_numbers.values()) >= SAVE_THRESHOLD:
                        enable_save = '1;'

                    return enable_save+text_to_show

            if request_key == 'save':
                save = int(request.values['save'])
                if save and predicted_numbers:
                    generated_filename = ''.join(random.sample(char_set*15, 15))
                    generated_filename += '.jpg'
                    db.insert_value(generated_filename, list(predicted_numbers.keys())[0])
                    with open('static/'+generated_filename, "wb") as image_file:
                        image_file.write(base64.b64decode(encoded_image))

    return render_template('canvas.html')


@app.route('/gallery')
def gallery():
    dict_of_filenames = db.get_keys_and_values()
    print(dict_of_filenames)
    return render_template('gallery.html', dict=dict_of_filenames)


def run_flask_server(host, port):
    app.run(host=host, port=port, debug=True)
