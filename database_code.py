from config import DATABASE_HOST, DATABASE_PORT
import redis


class DataBase:
    def __init__(self, host=DATABASE_HOST, port=DATABASE_PORT):
        self.db = redis.Redis(host=host, port=port)

    def insert_value(self, key, value):
        self.db.set(key, value)

    def get_keys_and_values(self):
        keys = self.db.keys()
        values = [elem.decode("utf-8") for elem in self.db.mget(keys)]
        keys = [elem.decode("utf-8") for elem in keys]
        records = {}
        for value, key in list(zip(keys, values)):
            if key in records.keys():
                records[key].append(value)
            else:
                records[key] = [value]
        return records
