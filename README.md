# Numbers detector
This app is used to determine which number you have drawn on the canvas.

## Launching using docker:
You can download docker from [Docker docs](https://docs.docker.com/get-docker/) and install it with using the instruction listed on the website.
After that you should clone repository and run docker-compose in the terminal:
```
git clone https://gitlab.com/Kolpnick/numbers-detector
cd numbers-detector
docker-compose up
```
Then, the app will be available on http://localhost:5000/ .

## Launching without docker:
You can launch this app without docker, if the following are installed:
* Python 3.6 or higher, available on [Python.org](https://www.python.org/)
* All modules listed in the [requirements file](requirements.txt)
* [Redis server](https://github.com/antirez/redis)

In the [config file](config.py) you can set host and port for the redis database (e.g. localhost:6379)

## The model used for predictions:
The model was trained on [the MNIST database of handwritten digits](https://storage.googleapis.com/tensorflow/tf-keras-datasets/mnist.npz) and have the following architecture:
**Model: "sequential"**
| Layer (type) | Output Shape |
| ----------- | ----------- |
| conv2d_1 (Conv2D) | (None, 28, 28, 32) |
| conv2d_2 (Conv2D) | (None, 28, 28, 32) |
| max_pooling2d_1 (MaxPooling2D) | (None, 14, 14, 32) |
| dropout_1 (Dropout)  | (None, 14, 14, 32) |
| conv2d_3 (Conv2D) | (None, 14, 14, 64) |
| conv2d_4 (Conv2D) | (None, 14, 14, 64) |
| max_pooling2d_2 (MaxPooling2D) | (None, 7, 7, 64) |
| dropout_2 (Dropout) | (None, 7, 7, 64) |
| flatten (Flatten) | (None, 3136) |
| dense_1 (Dense) | (None, 256) |
| dropout_3 (Dropout) | (None, 256) |
| dense_2 (Dense) | (None, 10) |   