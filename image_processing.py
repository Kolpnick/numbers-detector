from config import MODEL_PRETRAINED, MODEL_FILENAME, BATCH, LOSS, METRICS, N_EPOCHS
import tensorflow as tf
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.layers import Dense, Conv2D, Dropout, Flatten, MaxPool2D
from tensorflow.keras.optimizers import RMSprop
from tensorflow.keras.utils import to_categorical
from PIL import Image, ImageChops
import numpy as np
import base64
import io
import os


class MNIST_model:
    def __init__(self, pretrained=MODEL_PRETRAINED, filename=MODEL_FILENAME):
        self.image_size = 28
        if pretrained and os.path.isfile(filename):
            try:
                self.model = load_model(filename)
            except Exception:
                self.create_model()
                self.fit_model()
        else:
            self.create_model()
            self.fit_model()

    def image_normalizing(self, image):
        image = image.reshape(self.image_size, self.image_size, 1)
        image = image.astype('float32')
        image /= 255
        return image

    def prepare_model_datasets(self):
        (X_train, y_train), (X_test, y_test) = tf.keras.datasets.mnist.load_data()

        X_train_list = []
        X_test_list = []
        for img_train in X_train:
            X_train_list.append(self.image_normalizing(img_train))

        for img_test in X_test:
            X_test_list.append(self.image_normalizing(img_test))

        X_train = np.array(X_train_list)
        X_test = np.array(X_test_list)

        y_train = to_categorical(y_train, num_classes=10)
        y_test = to_categorical(y_test, num_classes=10)
        return (X_train, y_train), (X_test, y_test)

    def create_model(self):
        input_shape = (self.image_size, self.image_size, 1)

        self.model = Sequential()
        self.model.add(Conv2D(filters=32, kernel_size=(5, 5), padding='Same',
                              activation='relu', input_shape=input_shape))
        self.model.add(Conv2D(filters=32, kernel_size=(5, 5), padding='Same',
                              activation='relu'))
        self.model.add(MaxPool2D(pool_size=(2, 2)))
        self.model.add(Dropout(0.25))
        self.model.add(Conv2D(filters=64, kernel_size=(3, 3), padding='Same',
                              activation='relu'))
        self.model.add(Conv2D(filters=64, kernel_size=(3, 3), padding='Same',
                              activation='relu'))
        self.model.add(MaxPool2D(pool_size=(2, 2), strides=(2, 2)))
        self.model.add(Dropout(0.25))
        self.model.add(Flatten())
        self.model.add(Dense(256, activation="relu"))
        self.model.add(Dropout(0.5))
        self.model.add(Dense(10, activation="softmax"))

    def fit_model(self, batch_size=BATCH, loss=LOSS, metrics=METRICS, n_epochs=N_EPOCHS):
        (X_train, y_train), (X_test, y_test) = self.prepare_model_datasets()
        optimizer = RMSprop(learning_rate=0.001, rho=0.9, epsilon=1e-08, decay=0.0)
        self.model.compile(optimizer=optimizer, loss=loss, metrics=metrics)
        self.model.fit(X_train, y_train, batch_size=batch_size, epochs=n_epochs,
                       validation_data=(X_test, y_test), verbose=1)
        self.model.save(MODEL_FILENAME)

    def convert_base64_to_np(self, encoded_image):
        decoded_image = base64.b64decode(encoded_image)
        image = Image.open(io.BytesIO(decoded_image))
        bg = Image.new(image.mode, image.size, (255,255,255,0))
        diff = ImageChops.difference(image, bg)
        diff = ImageChops.add(diff, diff, 2.0, -100)
        bbox = diff.getbbox()

        if bbox:
            bbox = [bbox[0] - 20, bbox[1] - 20, bbox[2] + 20, bbox[3] + 20]
            bbox_width = bbox[2] - bbox[0]
            bbox_height = bbox[3] - bbox[1]
            size_diff = bbox_width - bbox_height
            if size_diff > 0:
                bbox[1] -= (size_diff // 2)
                bbox[3] += (size_diff - size_diff // 2)
            else:
                bbox[0] += (size_diff // 2)
                bbox[2] -= (size_diff - size_diff // 2)
            image = image.crop(bbox)

        image = image.resize((self.image_size, self.image_size))
        image_np = np.array(image)
        image_np = image_np[:, :, 3]
        image_np = image_np.astype('float32')
        image_np /= 255
        image_np = image_np.reshape(1, self.image_size, self.image_size, 1)
        return image_np

    def predict(self, encoded_image):
        image_np = self.convert_base64_to_np(encoded_image)
        pred = self.model.predict(image_np)[0]
        result = {i: 100*pred[i] for i in range(len(pred))}
        result = {k: v for k, v in sorted(result.items(), key=lambda item: item[1], reverse=True)}
        return result
