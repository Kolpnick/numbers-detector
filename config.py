# database configs
DATABASE_HOST = 'redis'
DATABASE_PORT = 6379
SAVE_THRESHOLD = 95

# flask configs
SHOW_THRESHOLD = 5
FLASK_HOST = '0.0.0.0'
FLASK_PORT = 5000

# model configs
MODEL_PRETRAINED = True
MODEL_FILENAME = 'pretrained_model.h5'
BATCH = 86
LOSS = 'categorical_crossentropy'
METRICS = ['accuracy']
N_EPOCHS = 10
