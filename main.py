from flask_code import run_flask_server
from config import FLASK_HOST, FLASK_PORT
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-host', required=False, default=FLASK_HOST, help='Site host')
    parser.add_argument('-port', required=False, default=FLASK_PORT, help='Site port')
    args = parser.parse_args()

    run_flask_server(args.host, args.port)
